#include "lib_button.h"

#define BT1_READ digitalRead(32)

TS_TypeInput    vrts_Button1;

void BUTTON_Init(void)
{
    pinMode(32, INPUT_PULLUP);
    vrts_Button1.vruc_FlagChange = 0;
}

void Button_Readall(void)
{
  Fn_INPUT_ReadInput (BT1_READ, (TS_TypeInput*)(&vrts_Button1));
}

void Fn_INPUT_ReadInput (unsigned char _vruc_Input, TS_TypeInput *_vrts_DataInput){
   _vrts_DataInput->vruc_DataOld = _vruc_Input;
  if(!_vruc_Input){
    if(_vrts_DataInput->vruc_DataOld == _vruc_Input){
      if(_vrts_DataInput->vruc_CountAccess <= MAXINPUT){
        _vrts_DataInput->vruc_CountAccess++;
        if(_vrts_DataInput->vruc_CountAccess == 20){
          _vrts_DataInput->vruc_FlagChange = 1;
        }
      }
    }
    else{
      _vrts_DataInput->vruc_CountAccess = 0;
    }
  }
  else{
    _vrts_DataInput->vruc_CountAccess = 0;
  }
}