#include "lib_eeprom.h"


void EEPROM_Init(void)
{
  if (EEPROM.begin(EEPROM_SIZE))
  {
    Serial.println("Failed To Initialise EEPROM"); 
  }
}

void EEPROM_Clear()
{
  // Xóa bộ nhớ EEPROM
  for (uint8_t i = 0; i < PHONE_SIZE; i++) 
  {
    EEPROM.write(i, 0xFF);
    EEPROM.commit();
  }
  EEPROM.end();
}

uint8_t EEPROM_Write(char *arr)
{
  uint8_t address = EEPROM_ADDRESS;
  for (uint8_t i = 0; i < PHONE_SIZE; i++)
  {
    EEPROM.writeByte(address, arr[i]);
    EEPROM.commit();
    address += 1;
  }
  return 1;
}

uint8_t EEPROM_Read(char *arr)
{
  uint8_t address = EEPROM_ADDRESS;
  for (uint8_t i = 0; i < PHONE_SIZE; i++)
  {
    arr[i] = EEPROM.readByte(address);
    address += 1;
  }
  return 1;
}
