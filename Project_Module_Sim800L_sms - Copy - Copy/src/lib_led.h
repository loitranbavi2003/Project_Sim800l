#ifndef _LIB_LED_H_
#define _LIB_LED_H_

#include "lib_sys.h"

#define PIN_LED_RED 25
#define PIN_LED_BLUE 26
#define PIN_LED_YELLOW 27

void LED_Init();
void LED_Red();
void LED_Blue();
void LED_Yellow();

#endif
