#include "lib_message_receive.h"


static char arr_receive[25];
static char start_frame[13];
static char check_phone[11];
static uint8_t flag_receive = 0;

uint8_t Message_Receive()
{
    String data_receive = Serial2.readStringUntil('\n');
    uint8_t length = data_receive.length() + 1; // Thêm kí hiệu '\0'
    data_receive.trim(); 
    // Serial.println(data_receive);
    // Serial.println(length);
    if(length == 24) // Serial : 23
    {
        data_receive.toCharArray(arr_receive, length);
        Serial.println();
        Serial.println((String)arr_receive);
        flag_receive = 1;
    }
    return 1;
}

uint8_t Detect_MessageReceive(char *data_in, char *data_out)
{
    // Phonenumber:0123456789
    for(uint8_t i = 0; i <= 11; i++)
    {
        start_frame[i] = data_in[i];
    }
    // Serial.println();
    // Serial.println((String)start_frame);
    for(uint8_t i = 0; i <= 9; i++)
    {
        data_out[i] = data_in[i+12];
        check_phone[i] = data_in[i+12];
    }
    // Serial.println((String)check_phone);
    if((String)start_frame != "Phonenumber:" || strlen(check_phone) != 10)
    {
        LED_Red();
        // Serial.println("Message Receive FIALED!");
        return 0;
    }
    return 1;
}

void Handle_MessageReceive()
{
    Message_Receive();
    if(flag_receive == 1)
    {
        char phone_number[11];
        if(Detect_MessageReceive(arr_receive, phone_number))
        {
            LED_Blue();
            Serial.println("Message True!");
            EEPROM_Write(phone_number);
            Serial.println((String)phone_number);
        }
        else
        {
            LED_Red();
            Serial.println("Message Fail!");
        }
        flag_receive = 0;
    }
}
