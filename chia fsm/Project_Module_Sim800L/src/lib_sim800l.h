#ifndef _LIB_SIM800L_H_
#define _LIB_SIM800L_H_

#include "lib_sys.h"

/*
  ESP32  | SIM 800L
 Rx - 16 |    Tx 
 Tx - 17 |    Rx
*/

void SIM800L_Setup();
void SIM800L_UpdateSerial();

#endif

/*
AT+CPAS     0: Đang không có cuộc gọi hoặc hoạt động.
            2: Đang trong trạng thái chờ (Idle).
            3: Đang trong trạng thái cuộc gọi đến (Incoming call ringing).
            4: Đang trong trạng thái cuộc gọi đi (Outgoing call dialing).
            5: Đang trong trạng thái cuộc gọi đang thiết lập (Call is being established).
            6: Đang trong trạng thái cuộc gọi bị từ chối (Call is being disconnected).
*/
