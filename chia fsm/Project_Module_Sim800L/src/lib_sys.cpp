#include "lib_sys.h"


hw_timer_t *My_timer = NULL;
fsm_status_e fsm_status = FSM_MESSAGERECEIVE;

static uint8_t count_press = 0;
static uint8_t flag_call_away = 0;
static uint8_t flag_call_incoming = 0;
static uint8_t flag_reset_call_incoming = 0;

static uint32_t count_time = 0;


void IRAM_ATTR onTimer()
{
  Button_Readall();
}

void SYS_Init()
{
  Serial.begin(115200);
  Serial2.begin(115200);
  LED_Init();
  TIMER_Init();
  BUTTON_Init(); 
  EEPROM_Init();
  SIM800L_Setup();
  SYS_CheckEeprom();
}

void SYS_Run()
{
  switch (fsm_status)
  {
    case FSM_MESSAGERECEIVE:
      count_time++;
      Handle_MessageReceive();

      if(count_time >= 200)
      {
        Serial2.print("AT+CPAS\r\n");

        if(Serial2.available())
        {
          String data_receive = Serial2.readStringUntil('\n');
          //Serial.println("Data_receive: " + data_receive);
          data_receive.trim(); // Xoa bo dau " " neu co

          if((data_receive == "+CPAS: 0") && (flag_check_message == 1) && (flag_call_away == 0))
          {
            Serial.println("Ready Call!");
            flag_call_away = 1;
            LED_Blue();
          }

          if(data_receive == "+CPAS: 3")
          {
            flag_call_incoming = 1;
            Serial.println("Co cuoc goi den!");
          }

          if(flag_call_incoming == 1)
          {
            if(data_receive == "+CPAS: 0")
            {
              flag_reset_call_incoming = 1;
              Serial.println("Cuoc goi den bi huy!");
            }
          }
        }
        Serial.println("FSM_MessageReceive");
        count_time = 0;
        fsm_status = FSM_CALL;
      }
    break;

    case FSM_CALL:
      if(vrts_Button1.vruc_FlagChange == 1)
      {
        count_press++;
        // Reset count_press 
        if(count_press == 10)
        {
          count_press = 0;
        }
      }

      if(vrts_Button1.vruc_FlagChange == 1)
      {
        if(count_press%2 == 0)
        {
          // Thuc hien lenh goi di
          if(flag_call_away == 1)
          {
            Serial.println("Calling");
            SYS_CallAway();
          }
          
        }
        else
        {
          if(flag_call_away == 1)
          {
            if (Serial2.available()) // Cuoc goi di dang ton tai
            {
              flag_call_away = 0;
              Serial.println("Huy cuoc goi!");
              Serial2.print("ATH\r\n");
              SIM800L_UpdateSerial();
              LED_Blue();
            }
          }
        }

        vrts_Button1.vruc_FlagChange = 0;
      }
      Serial.println("FSM_Call");
      fsm_status = FSM_MESSAGERECEIVE;
    break;
  }
  delay(1);
}

void SYS_CallAway()
{
  char phone_number[10];
  LED_Yellow();
  EEPROM_Read(phone_number);
  Serial.print("Dang goi den so: ");
  Serial.println((String)phone_number);
  Serial2.print("ATD" + (String)phone_number + ";\r\n");
}

void SYS_CallIncoming()
{
  if(vrts_Button1.vruc_FlagChange == 1)
  {
    flag_call_incoming = 0;
    vrts_Button1.vruc_FlagChange = 0;
    Serial.println("Chap nhan cuoc goi den!");
    Serial2.println("ATA\r\n");
    SIM800L_UpdateSerial();
    LED_Yellow();
  }
}

void SYS_CheckEeprom()
{
  char check_phone[11];
  EEPROM_Read(check_phone);
  //Serial.println(check_phone);
  if(strlen(check_phone) == 10)
  {
    LED_Blue();
  }
  else
  {
    LED_Red();
  }
}

void TIMER_Init(void)
{
  My_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(My_timer, &onTimer, true);
  timerAlarmWrite(My_timer, 1000, true);
  timerAlarmEnable(My_timer);
}
