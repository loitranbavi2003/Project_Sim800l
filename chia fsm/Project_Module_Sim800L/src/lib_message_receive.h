#ifndef _LIB_DETECT_H_
#define _LIB_DETECT_H_

#include "lib_sys.h"

extern uint8_t flag_check_message;

uint8_t Message_Receive();
void Handle_MessageReceive();
uint8_t Detect_MessageReceive(char *data_in, char *data_out);


#endif
