#ifndef _LIB_SYS_H_
#define _LIB_SYS_H_

#include <Arduino.h>
#include <EEPROM.h>
#include <string.h>

#include "stdint.h"
#include "lib_led.h"
#include "lib_button.h"
#include "lib_eeprom.h"
#include "lib_sim800l.h"
#include "lib_message_receive.h"


typedef enum
{
      FSM_MESSAGERECEIVE = 1,
      FSM_CALL,
} fsm_status_e;

void SYS_Init();
void SYS_Run();
void SYS_CallAway();
void SYS_CallIncoming();
void SYS_CheckEeprom();

void TIMER_Init(void);

#endif
