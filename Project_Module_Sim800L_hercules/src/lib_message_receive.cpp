#include "lib_message_receive.h"


static char arr_receive[20];
static char start_frame[7];
static char check_phone[11];
static uint8_t flag_receive = 0;

uint8_t Message_Receive()
{
    if (Serial.available()) 
    {
        String data_receive = Serial.readStringUntil('\n');
        uint8_t length = data_receive.length() + 1;// cộng thêm 1 để lưu kí tự kết thúc chuỗi '\0'
        if(length == 17)
        {
            data_receive.toCharArray(arr_receive, length);
            Serial.println();
            // Serial.println((String)arr_receive);
            flag_receive = 1;
        }
        else
        {
            LED_Red();
            Serial.println();
            Serial.println("Message Receive FIALED!");
            return 0;
        }
    }
    return 1;
}

uint8_t Detect_MessageReceive(char *data_in, char *data_out)
{
    // Phone:0123456789
    for(uint8_t i = 0; i <= 5; i++)
    {
        start_frame[i] = data_in[i];
    }
    // Serial.println();
    // Serial.println((String)start_frame);
    for(uint8_t i = 0; i <= 9; i++)
    {
        data_out[i] = data_in[i+6];
        check_phone[i] = data_in[i+6];
    }
    // Serial.println((String)check_phone);
    if((String)start_frame != "Phone:" || strlen(check_phone) != 10)
    {
        return 0;
    }
    return 1;
}

void Handle_MessageReceive()
{
    Message_Receive();
    if(flag_receive == 1)
    {
        char phone_number[10];
        if(Detect_MessageReceive(arr_receive, phone_number))
        {
                LED_Blue();
                Serial.println("Message True!");
                EEPROM_Write(phone_number);
                //Serial.println((String)phone_number);
        }
        else
        {
                LED_Red();
                Serial.println("Message Fail!");
        }
        flag_receive = 0;
    }
}
