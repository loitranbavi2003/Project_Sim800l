#ifndef _LIB_EEPROM_H_
#define _LIB_EEPROM_H_

#include "lib_sys.h"

#define PHONE_SIZE 10
#define EEPROM_SIZE 20
#define EEPROM_ADDRESS 0

void EEPROM_Init();
uint8_t EEPROM_Read(char *arr);
uint8_t EEPROM_Write(char *arr);

#endif
