#include "lib_led.h"


void LED_Init()
{
      pinMode(PIN_LED_RED, OUTPUT);
      pinMode(PIN_LED_BLUE, OUTPUT);
      pinMode(PIN_LED_YELLOW, OUTPUT);
      digitalWrite(PIN_LED_RED, 1);
      digitalWrite(PIN_LED_BLUE, 1);
      digitalWrite(PIN_LED_YELLOW, 1);
}

void LED_Red()
{
      digitalWrite(PIN_LED_RED, 0);
      digitalWrite(PIN_LED_BLUE, 1);
      digitalWrite(PIN_LED_YELLOW, 1);
}

void LED_Blue()
{
      digitalWrite(PIN_LED_RED, 1);
      digitalWrite(PIN_LED_BLUE, 0);
      digitalWrite(PIN_LED_YELLOW, 1);
}

void LED_Yellow()
{
      digitalWrite(PIN_LED_RED, 1);
      digitalWrite(PIN_LED_BLUE, 1);
      digitalWrite(PIN_LED_YELLOW, 0);
}

