#ifndef _LIB_SIM800L_H_
#define _LIB_SIM800L_H_

#include "lib_sys.h"

/*
  ESP32  | SIM 800L
 Rx - 16 |    Tx 
 Tx - 17 |    Rx
*/

void SIM800L_Call();
void SIM800L_Setup();
void SIM800L_UpdateSerial();

#endif

/*
AT+CPAS    
      0: Sẵn sàng cho cuộc gọi.
      2: Không đáp ứng.
      3: Đang trong trạng thái có cuộc gọi đến.
      4: Đang trong trạng thái cuộc gọi đi.

AT+CLCC:
      +CLCC: <id>,<dir>,<state>,<mode>,<mpty>,"<number>",<type>

      <id>: ID của cuộc gọi (số nguyên dương).
      <dir>: Hướng cuộc gọi (0: Gọi đi, 1: Gọi đến).
      <state>: Trạng thái cuộc gọi (0: Đã chấp nhận cuộc gọi, 1: Đang thiết lập, 2: Đang kết nối, 3: Đã kết nôi, 4: Gọi nhỡ, 5: Chờ giữ, 6: Kết thúc).
      <mode>: Chế độ cuộc gọi (0: Voice, 1: Data, 2: Fax).
      <mpty>: Trạng thái cuộc gọi nhiều bên (0: Không phải cuộc gọi nhiều bên, 1: Là cuộc gọi nhiều bên).
      "<number>": Số điện thoại liên quan đến cuộc gọi (nếu có).
      <type>: Loại số điện thoại (0: Quốc tế, 1: Quốc gia, 2: Dịch vụ).
      Khi không có cuộc gọi nào đang hoạt động, phản hồi sẽ là "OK" hoặc "+CLCC: 0".
*/
