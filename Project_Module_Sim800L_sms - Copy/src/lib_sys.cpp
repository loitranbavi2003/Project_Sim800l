#include "lib_sys.h"


value_t var_sys;
hw_timer_t *My_timer = NULL;

static uint8_t count_press = 0;
static uint8_t flag_call_away = 0;
static uint8_t flag_call_accept = 0;
static uint8_t flag_check_message = 0;
static uint8_t flag_call_incoming = 0;
static uint8_t flag_count_time_call = 0;

void IRAM_ATTR onTimer()
{
      Button_Readall();

      if(var_sys.count_time <= 200)
      {
            var_sys.count_time++;
      }
      // Đếm thời gian gọi chờ
      if(flag_count_time_call == 1)
      {
            var_sys.count_time_call++;
      }
}

void SYS_Init()
{
      Serial.begin(115200);
      Serial2.begin(115200);
      LED_Init();
      TIMER_Init();
      BUTTON_Init(); 
      EEPROM_Init();
      // EEPROM_Clear();
      SIM800L_Setup();
}

void SYS_Run()
{
      // Kiểm tra xem số trong bộ nhớ EEPROM có đúng dạng không
      SYS_CheckEeprom();
      
      // Kiểm tra Module Sim đã sẵn sàng gọi
      if((flag_check_message == 1) && (flag_call_away == 0))
      {
            LED_Blue();
            Serial.println("Call Ready!");
            flag_call_away = 1;
      }

      // Đọc bản tin gửi về của Module Sim800L
      if(var_sys.count_time >= 200)
      {
            Serial2.print("AT+CLCC\r\n"); // Kiểm tra trạng thái của cuộc gọi

            if(Serial2.available())
            {
                  // Hàm xử lý tin nhắn được gửi đến từ điện thoại
                  Handle_MessageReceive();

                  char arr_receive[40];
                  String data_receive = Serial2.readStringUntil('\n');  // +CLCC: 1,1,4,0,0,"0123456789",129,""
                  uint8_t length_data = data_receive.length() + 1;      // Thêm '\0'
                  data_receive.trim();                                  // Xóa bỏ dấu "cách" nếu có

                  if(data_receive.length() == 36)
                  {
                        data_receive.toCharArray(arr_receive, length_data);   // Chuyển String sang mảng Char
                        Serial.println("------------ DATA RECEIVE ----------");

                        // Xử lý cuộc gọi đến
                        if(arr_receive[9] == '1')
                        {
                              flag_call_incoming = 1;
                              Serial.println("Co cuoc goi den!");
                        }

                        // Cuộc gọi đi được chấp nhận
                        if(arr_receive[11] == '0')
                        {
                              flag_call_accept = 1;
                              flag_count_time_call = 0;
                              var_sys.count_time_call = 0;
                              Serial.println("Cuoc goi duoc chap nhan!");
                        }
                  }

                  if(flag_call_accept == 1)
                  {
                        Serial2.print("AT+CPAS\r\n");
                        String data = Serial2.readStringUntil('\n');
                        data.trim();                                  // Xóa bỏ dấu "cách" nếu có
                        // Serial.println(data);

                        if(data == "+CPAS: 0")
                        {
                              count_press = 1;
                              flag_call_away = 0;
                              flag_call_accept = 0;
                              Serial2.print("ATH\r\n");
                              SIM800L_UpdateSerial();
                              Serial.println("Cuoc goi di ket thuc!");
                        }
                  }
            }
            var_sys.count_time = 0;
      }

      // Xử lý cuộc gọi đi
      if(flag_call_away == 1)
      {
            Handle_CallAway();
      }
      // Hủy cuộc gọi đi khi quá thời gian chờ
      if(var_sys.count_time_call >= TIME_WAIT)
      {
            count_press = 1;
            flag_call_away = 0;
            flag_count_time_call = 0;
            var_sys.count_time_call = 0;
            Serial.println("Cuoc goi di khong thanh cong!");
            Serial2.print("ATH\r\n");
            SIM800L_UpdateSerial();
      }

      // Xử lý cuộc gọi đến
      if(flag_call_incoming == 1)
      {
            Serial.println("Huy cuoc goi den!");
            Serial2.print("ATH\r\n");
            SIM800L_UpdateSerial();
            flag_call_incoming = 0;
      }
}

void Handle_CallAway()
{
      // Đọc trạng thái của nút nhấn
      if(vrts_Button1.vruc_FlagChange == 1)
      {
            count_press++;

            if(count_press%2 == 0)
            {
                  flag_count_time_call = 1;
                  SIM800L_Call(); // Thực hiện cuộc gọi đi
            }
            else
            {
                  if (Serial2.available()) // Cuộc gọi đi đang tồn tại
                  {
                        count_press = 1;
                        flag_call_away = 0;
                        var_sys.count_time_call = 0;
                        Serial.println("Huy cuoc goi di!");
                        Serial2.print("ATH\r\n");
                        SIM800L_UpdateSerial();
                        LED_Blue();
                  }
            }
            vrts_Button1.vruc_FlagChange = 0;
      }
}

void SYS_CheckEeprom()
{
      char check_phone[11];
      EEPROM_Read(check_phone);
      //Serial.println(check_phone);
      if((check_phone[0] == '0') && (check_phone[1] != '0'))
      {
            flag_check_message = 1;
      }
      else
      {
            flag_check_message = 0;
            LED_Red();
      }
}

void TIMER_Init(void)
{
      My_timer = timerBegin(0, 80, true);
      timerAttachInterrupt(My_timer, &onTimer, true);
      timerAlarmWrite(My_timer, 1000, true);
      timerAlarmEnable(My_timer);
}
