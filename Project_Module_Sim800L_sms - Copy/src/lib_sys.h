#ifndef _LIB_SYS_H_
#define _LIB_SYS_H_

#include <Arduino.h>
#include <EEPROM.h>
#include <string.h>

#include "stdint.h"
#include "lib_led.h"
#include "lib_button.h"
#include "lib_eeprom.h"
#include "lib_sim800l.h"
#include "lib_message_receive.h"

#define TIME_WAIT 25000

typedef struct 
{
    uint32_t count_time;
    uint32_t count_time_call;
    uint32_t count_time_blink;
} value_t;
extern value_t var_sys;

void TIMER_Init(void);

void SYS_Init();
void SYS_Run();
void Handle_CallAway();
void SYS_CheckEeprom();

#endif
